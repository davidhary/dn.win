# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [1.1.9083] - 2024-11-13 Preview 202304
* Update packages.
* Upgrade Forms libraries and Test and Demo apps to .Net 9.0.
* Increment version to 9083.

## [1.1.8956] - 2024-07-09 Preview 202304
* References updated core projects.

## [1.1.8949] - 2024-07-02 Preview 202304
* Update to .Net 8.
* Implement MS Test SDK project format.
* Increment libraries versions.
* Use ordinal instead of ordinal ignore case string comparisons.
* Condition auto generating binding redirects on .NET frameworks 472 and 480.
* Apply code analysis rules.
* Generate assembly attributes.

## [1.1.8537] - 2023-05-17 Preview 202304
* Set test framework to .NET 7.
* Use Observable object form the community toolkit.
* Implement nullable.
* Rearrange folders.
* Set MSTest namespace to cc.isr.
* Fix orphan links in MD files. 
* update project references due to restructuring folders or referenced libraries.
* Update packages. 
* Use cc.isr.Json.AppSettings.ViewModels project.

## [1.1.8535] - 2023-05-15 Preview 202304
* Use cc.isr.Json.AppSettings.ViewModels project for settings I/O.

## [1.1.8518] - 2023-04-28 Preview 202304
* Split README.MD to attribution, cloning, open-source and read me files.
* Add code of conduct, contribution and security documents.
* Increment version.

## [1.0.8125] - 2022-03-31
* Pass tests in project reference mode. 

## [1.0.8119] - 2022-03-25
* Use the new Json application settings base class.

## [1.0.8110] - 2022-03-16
* Use the ?. operator, without making a copy of the delegate, 
to check if a delegate is non-null and invoke it in a thread-safe way.

## [1.0.8104] - 2022-03-10
* Forked from [dn.core].

&copy; 2012 Integrated Scientific Resources, Inc. All rights reserved.

[dn.core]: https://www.bitbucket.org/davidhary/dn.core
[1.1.9083]: https://bitbucket.org/davidhary/dn.win/src/main/
