# Open Source

Support libraries extending the Visual Studio framework for engineering and measurements.

* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Open-Source"></a>
## Open Source
Open source used by this software is described and licensed at the
following sites:  
[win]  
[json]  
[tracing]  
[Exception Extension]  

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  

[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[win]: https://www.bitbucket.org/davidhary/dn.win
[json]: https://www.bitbucket.org/davidhary/dn.json
[tracing]: https://www.bitbucket.org/davidhary/dn.tracing
[Exception Extension]: https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc
[Safe Copy from Clipboard]: http://stackoverflow.com/questions/899350/how-to-copy-the-contents-of-a-string-to-the-clipboard-in-c

[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide
[external repositories]: ExternalReposCommits.csv

