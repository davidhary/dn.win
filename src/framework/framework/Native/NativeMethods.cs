using System;
using System.Runtime.InteropServices;

#pragma warning disable IDE0079

namespace cc.isr.Win.Win32;

/// <summary>   A native methods. </summary>
/// <remarks>   David, 2020-12-05. </remarks>
internal class NativeMethods
{
    /// <summary>   Closes a handle. Frees the kernel's file object (close the file). </summary>
    /// <remarks>   David, 2020-12-05. </remarks>
    /// <param name="handle">   The handle. </param>
    /// <returns>   An Integer. </returns>
    [DllImport( "kernel32", SetLastError = true )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Interoperability", "SYSLIB1054:Use 'LibraryImportAttribute' instead of 'DllImportAttribute' to generate P/Invoke marshalling code at compile time", Justification = "<Pending>" )]
    internal static extern bool CloseHandle( IntPtr handle );

}
