using System;

namespace cc.isr.Win;

/// <summary> Information about the program. </summary>
/// <remarks> David, 2020-09-15. </remarks>
public class ProgramInfo
{
    #region " construction "

    /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    public ProgramInfo() : base() => this.Lines = [];

    #endregion

    #region " display "

    /// <summary> Gets or sets the lines. </summary>
    /// <value> The lines. </value>
    public System.Collections.ObjectModel.Collection<ProgramInfoLine> Lines { get; private set; }

    /// <summary> Clears this object to its blank/initial state. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    public void Clear()
    {
        this.Lines.Clear();
    }

    /// <summary> Appends a line. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="text"> The text. </param>
    /// <param name="font"> The font. </param>
    public void AppendLine( string text, System.Drawing.Font font )
    {
        this.Lines.Add( new ProgramInfoLine( text, font ) );
    }

    /// <summary> Updates the display contents. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="headerFont">  The header font. </param>
    /// <param name="contentFont"> The content font. </param>
    public void BuildContents( System.Drawing.Font headerFont, System.Drawing.Font contentFont )
    {
        Std.AssemblyInfo info = new();
        this.Clear();
        this.AppendLine( "Product Name:", headerFont );
        this.AppendLine( info.Product, contentFont );
        this.AppendLine( string.Empty, contentFont );
        this.AppendLine( "Product Title:", headerFont );
        this.AppendLine( info.Title, contentFont );
        this.AppendLine( string.Empty, contentFont );
        this.AppendLine( "File Name:", headerFont );
        this.AppendLine( info.FileVersionInfo.OriginalFilename ?? string.Empty, contentFont );
        this.AppendLine( string.Empty, contentFont );
        this.AppendLine( "Product Version:", headerFont );
        this.AppendLine( info.AssemblyVersion.ToString(), contentFont );
        this.AppendLine( string.Empty, contentFont );
        this.AppendLine( "Product Folder:", headerFont );
        this.AppendLine( info.DirectoryPath, contentFont );
        this.AppendLine( string.Empty, contentFont );
        this.AppendLine( "Program Data Folder:", headerFont );
        this.AppendLine( KnownFolders.GetPath( KnownFolder.ProgramData ) ?? string.Empty, contentFont );
        this.AppendLine( string.Empty, contentFont );
        this.AppendLine( "User Program Data Folder:", headerFont );
        this.AppendLine( KnownFolders.GetPath( KnownFolder.LocalAppData ) ?? string.Empty, contentFont );
        this.AppendLine( string.Empty, contentFont );
        System.Configuration.Configuration configuration = System.Configuration.ConfigurationManager.OpenExeConfiguration( System.Configuration.ConfigurationUserLevel.None );
        this.AppendLine( "All Users Configuration File:", headerFont );
        this.AppendLine( configuration.FilePath, contentFont );
        if ( configuration.Locations.Count > 0 )
        {
            this.AppendLine( "Configuration Locations:", headerFont );
            this.AppendLine( configuration.FilePath, contentFont );
            foreach ( System.Configuration.ConfigurationLocation s in configuration.Locations )
            {
                this.AppendLine( s.Path, contentFont );
            }
        }

        this.AppendLine( string.Empty, contentFont );
        configuration = System.Configuration.ConfigurationManager.OpenExeConfiguration( System.Configuration.ConfigurationUserLevel.PerUserRoaming );
        this.AppendLine( "User Roaming Configuration File:", headerFont );
        this.AppendLine( configuration.FilePath, contentFont );
        if ( configuration.Locations.Count > 0 )
        {
            this.AppendLine( "Configuration Locations:", headerFont );
            this.AppendLine( configuration.FilePath, contentFont );
            foreach ( System.Configuration.ConfigurationLocation s in configuration.Locations )
            {
                this.AppendLine( s.Path, contentFont );
            }
        }

        this.AppendLine( string.Empty, contentFont );
        _ = System.Configuration.ConfigurationManager.OpenExeConfiguration( System.Configuration.ConfigurationUserLevel.PerUserRoamingAndLocal );
        configuration = System.Configuration.ConfigurationManager.OpenExeConfiguration( System.Configuration.ConfigurationUserLevel.PerUserRoaming );
        this.AppendLine( "User Local  Configuration File:", headerFont );
        this.AppendLine( configuration.FilePath, contentFont );
        if ( configuration.Locations.Count > 0 )
        {
            this.AppendLine( "Configuration Locations:", headerFont );
            this.AppendLine( configuration.FilePath, contentFont );
            foreach ( System.Configuration.ConfigurationLocation s in configuration.Locations )
            {
                this.AppendLine( s.Path, contentFont );
            }
        }

        this.AppendLine( string.Empty, contentFont );

        // allows the calling application to add data.
        this.OnProgramInfoRequested( EventArgs.Empty );
        this.AppendLine( "Product Copyrights:", headerFont );
        this.AppendLine( info.Copyright, contentFont );
        this.AppendLine( string.Empty, contentFont );
    }

    #endregion

    #region " program info requested "

    /// <summary> Notifies of the <see cref="ProgramInfoRequested"/> event. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="e"> Event information to send to registered event handlers. </param>
    protected virtual void OnProgramInfoRequested( EventArgs e )
    {
        this.NotifyProgramInfoRequested( e );
    }

    /// <summary> Event queue for all listeners interested in  <see cref="ProgramInfoRequested"/> events. </summary>
    public event EventHandler<EventArgs>? ProgramInfoRequested;

    /// <summary>
    /// Synchronously invokes the <see cref="ProgramInfoRequested">ProgramInfoRequested Event</see>.
    /// Not thread safe.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="e">    The <see cref="EventArgs" /> instance containing the event data. </param>
    protected void NotifyProgramInfoRequested( EventArgs e )
    {
        this.ProgramInfoRequested?.Invoke( this, e );
    }

    #endregion

    #region " refresh requested "

    /// <summary> Notifies of the <see cref="RefreshRequested"/> event. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    public void NotifyRefreshRequested()
    {
        this.NotifyRefreshRequested( EventArgs.Empty );
    }

    /// <summary> Event queue for all listeners interested in <see cref="RefreshRequested"/> events. </summary>
    public event EventHandler<EventArgs>? RefreshRequested;

    /// <summary>
    /// Synchronously invokes the <see cref="RefreshRequested">RefreshRequested Event</see>. Not
    /// thread safe.
    /// </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    /// <param name="e">    The <see cref="EventArgs" /> instance containing the event data. </param>
    protected void NotifyRefreshRequested( EventArgs e )
    {
        this.RefreshRequested?.Invoke( this, e );
    }

    #endregion
}
/// <summary> A program information line. </summary>
/// <remarks> David, 2020-09-15. </remarks>
/// <remarks> Constructor. </remarks>
/// <remarks> David, 2020-09-15. </remarks>
/// <param name="text"> The text. </param>
/// <param name="font"> The font. </param>
public struct ProgramInfoLine( string text, System.Drawing.Font font )
{
    /// <summary> Gets or sets the font. </summary>
    /// <value> The font. </value>
    public System.Drawing.Font Font { get; set; } = font;

    /// <summary> Gets or sets the text. </summary>
    /// <value> The text. </value>
    public string Text { get; set; } = text;
}
