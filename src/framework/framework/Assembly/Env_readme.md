### dotnet-env

A .NET class to load environment variables from .env files. 

#### Source

* [DotNetEnv](https://github.com/tonerdo/dotnet-env) - Dot Net Environment original project

#### Changes from Source

* Get functions changed to Retrieve functions
* Retrieve function accept nullable default values and return nullable values without breaking code compatibility.
* VB.Net examples added below.

#### Usage

##### Load .env file

`Load()` expects an `.env` file in the current directory

###### C#
```
Env.Load();
```

###### VB.Net
```
Env.Load()
```

Or you can specify the path to the `.env` file
###### C#
```
Env.Load("./path/to/.env");
```

###### VB.Net
```
Env.Load("./path/to/.env")
```

It's also possible to load the (text) file as a `Stream`

###### C#
```
using (var stream = File.OpenRead("./path/to/.env"))
{
    Env.Load(stream);
}
```

###### VB.Net
```
Using stream as Stream= File.OpenRead("./path/to/.env"))
    Env.Load(stream)
End Using
```

##### Accessing environment variables

The variables in the `.env` can then be accessed through the `System.Environment` class

###### C#
```
System.Environment.GetEnvironmentVariable("IP");
```

###### VB.Net
```
System.Environment.GetEnvironmentVariable("IP")
```
Or through on of the helper methods:

###### C#
```
Env.Parse("A_STRING");
Env.Parse("A_BOOL");
Env.Parse("AN_INT");
Env.Parse("A_DOUBLE");
```

###### VB.Net
```
Env.Parse("A_STRING")
Env.Parse("A_BOOL")
Env.Parse("AN_INT")
Env.Parse("A_DOUBLE")
```

The helper methods also has a optional second argument which specifies what value to return if the variable is not found:

#### C#
```
Env.Parse("THIS_DOES_NOT_EXIST", "Variable not found");
```

#### VB.Net
```
Env.Parse("THIS_DOES_NOT_EXIST", "Variable not found")
```

### Additional arguments

You can also pass a LoadOptions object arg to all Env.Load variants to affect the Load/Parse behavior:

#### C#
```
new Env.LoadOptions(
    trimWhitespace: false,
    isEmbeddedHashComment: false,
    unescapeQuotedValues: false,
    clobberExistingValues: false
)
```

#### VB.Net
```
new Env.LoadOptions(
    trimWhitespace:= false,
    isEmbeddedHashComment:= false,
    unescapeQuotedValues:= false,
    clobberExistingValues:= false)
```

All parameters default to true, which means:

##### `trimWhitespace = True`
Toggles trimming white space.

###### env file contents:
```
KEY  =  value
```

###### C#
```
"value" == System.Environment.GetEnvironmentVariable("KEY")
null == System.Environment.GetEnvironmentVariable("  KEY  ")
```

##### `trimWhitespace = False`
###### C#
```
"  value" == System.Environment.GetEnvironmentVariable("  KEY  ")
null == System.Environment.GetEnvironmentVariable("KEY")
```

##### `isEmbeddedHashComment = True`
Toggles allowing inline comments

###### env file contents:
```
KEY=value  ### comment
```

Would then be available as
###### C#
```
"value" == System.Environment.GetEnvironmentVariable("KEY")
```

##### `isEmbeddedHashComment = False`
###### C#
```
"value  ### comment" == System.Environment.GetEnvironmentVariable("KEY")
```

Which is most useful when you want to do something like:
###### env file content
```
KEY=value#moreValue#otherValue#etc
```

##### `unescapeQuotedValues = True`

Toggles unescape/parse quoted (single or double) values as being strings with escaped chars
such as newline ("\n"), but also handles Unicode chars (e.g. "\u00ae" and "\U0001F680") -- note that you can always include unescaped Unicode chars anyway (e.g. "日本") if your .env is in UTF-8. Also note that there is no need to escape quotes inside.

###### env file contents:
```
KEY="quoted\n\tvalue"
```

###### C#
```
"quoted
    value" == System.Environment.GetEnvironmentVariable("KEY")
```

##### `unescapeQuotedValues = False`
###### C#
```
"\"quoted\\n\\tvalue\"" == System.Environment.GetEnvironmentVariable("KEY")
```

##### `clobberExistingValues = True`
toggles overwriting existing environment variables

###### env file contents:
```
KEY=value
```

###### C#
```
System.Environment.SetEnvironmentVariable("KEY", "really important value, don't overwrite");
Env.Load(
    new Env.LoadOptions(
        clobberExistingValues: false
    )
)
"really important value, don't overwrite" == System.Environment.GetEnvironmentVariable("KEY")
```
Note that the values of the environment variable did not come from the environment file.

#### License

This repository is licensed under the MIT license.
