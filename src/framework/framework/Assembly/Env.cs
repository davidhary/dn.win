using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace cc.isr.Win;

/// <summary> An environment. </summary>
/// <remarks>
/// (c) 2019 TONERDO. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-10-16  </para><para>
/// https://GitHub.com/tonerdo/dotnet-env. </para>
/// </remarks>
public sealed class Env
{
    #region " construction "

    /// <summary>
    /// Constructor that prevents a default instance of this class from being created.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    private Env() : base()
    { }

    /// <summary>   (Immutable) The default environment file name. </summary>
    /// <remarks>   This is due to the VS bug causing VS to ignore the underscore rule. </remarks>
#pragma warning disable IDE0079
#pragma warning disable CA1707
    public const string DEFAULT_ENV_FILENAME = ".env";
#pragma warning restore CA1707
#pragma warning restore IDE0079

    /// <summary> Loads the given options. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="lines">   The lines. </param>
    /// <param name="options"> The options to load. </param>
    public static void Load( string[] lines, EnvLoadOptions options )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( options, nameof( options ) );
#else
        if ( options is null )
        {
            throw new ArgumentNullException( nameof( options ) );
        }
#endif

        EnvironmentVariableDictionary envFile = Parser.Parse( lines, options.TrimWhiteSpace, options.IsEmbeddedHashComment, options.UnEscapeQuotedValues );
        envFile.SetEnvironmentVariables( options.ClobberExistingValues );
    }

    /// <summary> Loads the given options. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="path">    Full pathname of the file. </param>
    /// <param name="options"> The options to load. </param>
    public static void Load( string path, EnvLoadOptions options )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( options, nameof( options ) );
#else
        if ( options is null )
        {
            throw new ArgumentNullException( nameof( options ) );
        }
#endif

        if ( !options.RequireEnvFile && !File.Exists( path ) ) return;

        Load( File.ReadAllLines( path ), options );
    }

    /// <summary> Loads the given options. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="file">    The file. </param>
    /// <param name="options"> The options to load. </param>
    public static void Load( Stream file, EnvLoadOptions options )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( options, nameof( options ) );
#else
        if ( options is null )
        {
            throw new ArgumentNullException( nameof( options ) );
        }
#endif

        List<string> lines = [];
        string? currentLine = string.Empty;
        using ( StreamReader reader = new( file ) )
        {
            while ( currentLine is not null )
            {
                currentLine = reader.ReadLine();
                if ( currentLine is not null )
                {
                    lines.Add( currentLine );
                }
            }
        }

        Load( [.. lines], options );
    }

    /// <summary> Loads the given options. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="options"> The options to load. </param>
    public static void Load( EnvLoadOptions options )
    {
        Load( Path.Combine( Directory.GetCurrentDirectory(), DEFAULT_ENV_FILENAME ), options );
    }

    #endregion

    #region " retrieve "

    /// <summary> Retrieves the environment value of the specified key. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="key">          The key. </param>
    /// <param name="defaultValue"> (Optional) The default value. </param>
    /// <returns> An Integer. </returns>
    public static string? RetrieveValue( string key, string? defaultValue = null )
    {
        return Environment.GetEnvironmentVariable( key ) ?? defaultValue;
    }

    /// <summary> Retrieves the environment value of the specified key. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="key">          The key. </param>
    /// <param name="defaultValue"> (Optional) The default value. </param>
    /// <returns> An Integer. </returns>
    public static bool? RetrieveBoolean( string key, bool? defaultValue = default )
    {
        string? keyValue = Environment.GetEnvironmentVariable( key );
        return bool.TryParse( keyValue, out bool parsedValue ) ? parsedValue : defaultValue;
    }

    /// <summary> Retrieves the environment value of the specified key. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="key">          The key. </param>
    /// <param name="defaultValue"> (Optional) The default value. </param>
    /// <returns> An Integer. </returns>
    public static double? RetrieveDouble( string key, double? defaultValue = default )
    {
        string? keyValue = Environment.GetEnvironmentVariable( key );
        return double.TryParse( keyValue, out double parsedValue ) ? parsedValue : defaultValue;
    }

    /// <summary> Retrieves the environment value of the specified key. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="key">          The key. </param>
    /// <param name="defaultValue"> (Optional) The default value. </param>
    /// <returns> An Integer. </returns>
    public static int? RetrieveInteger( string key, int? defaultValue = default )
    {
        string? keyValue = Environment.GetEnvironmentVariable( key );
        return int.TryParse( keyValue, out int parsedValue ) ? parsedValue : defaultValue;
    }

    #endregion

    #region " dictionary "

    /// <summary> Dictionary of environment variables. </summary>
    /// <remarks>
    /// (c) 2019 TONERDO. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-10-16 </para><para>
    /// https://GitHub.com/tonerdo/dotnet-env. </para>
    /// </remarks>
    private class EnvironmentVariableDictionary : Dictionary<string, string>
    {
        /// <summary> Sets environment variables. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="clobberExistingVars"> (Optional) True to clobber existing variables. </param>
        public void SetEnvironmentVariables( bool clobberExistingVars = true )
        {
            foreach ( KeyValuePair<string, string> keyValuePair in this )
            {
                if ( clobberExistingVars || Environment.GetEnvironmentVariable( keyValuePair.Key ) is null )
                {
                    Environment.SetEnvironmentVariable( keyValuePair.Key, keyValuePair.Value );
                }
            }
        }
    }

    #endregion

    #region " parser "

    /// <summary> A parser. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-10-16 </para><para>
    ///  https://GitHub.com/tonerdo/dotnet-env. </para>
    /// </remarks>
    private sealed class Parser
    {
        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        private Parser() : base()
        {
        }

        // Disable the warning.
#pragma warning disable IDE0079
#pragma warning disable SYSLIB1045

        /// <summary> The export RegEx. </summary>
        private static readonly Regex _exportRegex = new( @"^\s*export\s+" );

        // Re-enable the warning.
#pragma warning restore SYSLIB1045
#pragma warning restore IDE0079


        /// <summary> Query if 'line' is comment. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="line"> The line. </param>
        /// <returns> <c>true</c> if comment; otherwise <c>false</c> </returns>
        private static bool IsComment( string line )
        {
            return line.Trim().StartsWith( "#", StringComparison.OrdinalIgnoreCase );
        }

        /// <summary> Removes the inline comment described by line. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="line"> The line. </param>
        /// <returns> A <see cref="string" />. </returns>
        private static string RemoveInlineComment( string line )
        {
            int pos = line.IndexOf( '#' );
            return pos >= 0 ? line[..pos] : line;
        }

        /// <summary> Removes the export keyword described by line. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="line"> The line. </param>
        /// <returns> A <see cref="string" />. </returns>
        private static string RemoveExportKeyword( string line )
        {
            Match match = _exportRegex.Match( line );
            return match.Success ? line[match.Length..] : line;
        }

        private static readonly char[] _separator = ['='];


        /// <summary> Parses. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="lines">                 The lines. </param>
        /// <param name="trimWhitespace">        (Optional) True to trim whitespace. </param>
        /// <param name="isEmbeddedHashComment"> (Optional) True if is embedded hash comment, false if
        /// not. </param>
        /// <param name="unEscapeQuotedValues">  (Optional) True to un-escape quoted values. </param>
        /// <returns> An EnvironmentVariableDictionary. </returns>
        public static EnvironmentVariableDictionary Parse( string[] lines, bool trimWhitespace = true, bool isEmbeddedHashComment = true, bool unEscapeQuotedValues = true )
        {
            EnvironmentVariableDictionary vars = new();
            for ( int i = 0, loopTo = lines.Length - 1; i <= loopTo; i++ )
            {
                string line = lines[i];

                // skip comments
                if ( IsComment( line ) )
                {
                    continue;
                }

                if ( isEmbeddedHashComment )
                {
                    line = RemoveInlineComment( line );
                }

                line = RemoveExportKeyword( line );
                string[] keyValuePair = line.Split( _separator, 2 );

                // skip malformed lines
                if ( keyValuePair.Length != 2 )
                {
                    continue;
                }

                if ( trimWhitespace )
                {
                    keyValuePair[0] = keyValuePair[0].Trim();
                    keyValuePair[1] = keyValuePair[1].Trim();
                }

                if ( unEscapeQuotedValues && IsQuoted( keyValuePair[1] ) )
                {
                    keyValuePair[1] = UnEscape( keyValuePair[1][1..^1] );
                }

                vars.Add( keyValuePair[0], keyValuePair[1] );
            }

            return vars;
        }

        /// <summary> Query if 's' is quoted. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="s"> The string. </param>
        /// <returns> <c>true</c> if quoted; otherwise <c>false</c> </returns>
        private static bool IsQuoted( string s )
        {
            return s.Length > 1 && ((string.IsNullOrEmpty( s[0].ToString() ) && string.IsNullOrEmpty( s[^1].ToString() )) || (s[0] == '\'' && s[^1] == '\''));
        }

        // Disable the warning.
#pragma warning disable IDE0079
#pragma warning disable SYSLIB1045

        /// <summary> The export RegEx. </summary>
        private static readonly Regex _unEscapeRegex = new( @"\\[abfnrtv?""'\\]|\\[0-3]?[0-7]{1,2}|\\u[0-9a-fA-F]{4}|\\U[0-9a-fA-F]{8}|." );

        // Re-enable the warning.
#pragma warning restore SYSLIB1045
#pragma warning restore IDE0079


        /// <summary> Removes escape notations. </summary>
        /// <remarks>
        /// copied from
        /// https://StackOverflow.com/questions/6629020/evaluate-escaped-string/25471811#25471811.
        /// </remarks>
        /// <param name="s"> The string. </param>
        /// <returns> A <see cref="string" />. </returns>
        private static string UnEscape( string s )
        {
            StringBuilder sb = new();
            Regex r = _unEscapeRegex;
            MatchCollection mc = r.Matches( s, 0 );
            foreach ( Match m in mc )
            {
                if ( m.Length == 1 )
                {
                    _ = sb.Append( m.Value );
                }
                else if ( m.Value[1] is >= '0' and <= '7' )
                {
                    int i = Convert.ToInt32( m.Value[1..], 8 );
                    _ = sb.Append( ( char ) i );
                }
                else if ( m.Value[1] == 'u' )
                {
                    int i = Convert.ToInt32( m.Value[2..], 16 );
                    _ = sb.Append( ( char ) i );
                }
                else if ( m.Value[1] == 'U' )
                {
                    int i = Convert.ToInt32( m.Value[2..], 16 );
                    _ = sb.Append( char.ConvertFromUtf32( i ) );
                }
                else
                {
                    switch ( m.Value[1] )
                    {
                        case 'a':
                            {
                                _ = sb.Append( @"\a" );
                                break;
                            }

                        case 'b':
                            {
                                _ = sb.Append( @"\b" );
                                break;
                            }

                        case 'f':
                            {
                                _ = sb.Append( @"\f" );
                                break;
                            }

                        case 'n':
                            {
                                _ = sb.Append( @"\n" );
                                break;
                            }

                        case 'r':
                            {
                                _ = sb.Append( @"\r" );
                                break;
                            }

                        case 't':
                            {
                                _ = sb.Append( @"\t" );
                                break;
                            }

                        case 'v':
                            {
                                _ = sb.Append( @"\v" );
                                break;
                            }

                        default:
                            {
                                _ = sb.Append( m.Value[1] );
                                break;
                            }
                    }
                }
            }

            return sb.ToString();
        }
    }

    #endregion
}
/// <summary> The <see cref="Env"/> load options. </summary>
/// <remarks>
/// (c) 2019 TONERDO. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-10-16 </para><para>
/// https://GitHub.com/tonerdo/dotnet-env. </para>
/// </remarks>
/// <remarks> Constructor. </remarks>
/// <remarks> David, 2020-09-15. </remarks>
/// <param name="trimWhiteSpace">        (Optional) True to trim white space. </param>
/// <param name="isEmbeddedHashComment"> (Optional) True if is embedded hash comment, false if
/// not. </param>
/// <param name="unEscapeQuotedValues">  (Optional) True to un-escape quoted values. </param>
/// <param name="clobberExistingValues"> (Optional) True to clobber existing values. </param>
/// <param name="requireEnvFile">        (Optional) True to require environment file. </param>
public class EnvLoadOptions( bool trimWhiteSpace = true, bool isEmbeddedHashComment = true, bool unEscapeQuotedValues = true, bool clobberExistingValues = true, bool requireEnvFile = true )
{
    /// <summary> Gets or sets the trim white space. </summary>
    /// <value> The trim white space. </value>
    public bool TrimWhiteSpace { get; private set; } = trimWhiteSpace;

    /// <summary> Gets or sets the is embedded hash comment. </summary>
    /// <value> The is embedded hash comment. </value>
    public bool IsEmbeddedHashComment { get; private set; } = isEmbeddedHashComment;

    /// <summary> Gets or sets the un-escape quoted values. </summary>
    /// <value> The un-escape quoted values. </value>
    public bool UnEscapeQuotedValues { get; private set; } = unEscapeQuotedValues;

    /// <summary> Gets or sets the clobber existing values. </summary>
    /// <value> The clobber existing values. </value>
    public bool ClobberExistingValues { get; private set; } = clobberExistingValues;

    /// <summary> Gets or sets the require environment file. </summary>
    /// <value> The require environment file. </value>
    public bool RequireEnvFile { get; private set; } = requireEnvFile;
}
