using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;

#pragma warning disable IDE0079

namespace cc.isr.Win;

/// <summary> The process methods. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-08-15 </para>
/// </remarks>
public sealed class ProcessMethods
{
    #region " construction and cleanup "

    /// <summary>
    /// Constructor that prevents a default instance of this class from being created.
    /// </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    private ProcessMethods() : base()
    { }

    #endregion

    #region " design mode "

    /// <summary>
    /// Gets a value indicating whether the application is running under the IDE in design mode.
    /// </summary>
    /// <value>
    /// <c>true</c> if the application is running under the IDE in design mode; otherwise,
    /// <c>false</c>.
    /// </value>
    public static bool InDesignMode => Debugger.IsAttached;

    /// <summary> List of names of the designer process. </summary>
    private static readonly string[] _designerProcessNames = ["xdesproc", "devenv"];

    /// <summary> True to running from visual studio designer? </summary>
    private static bool? _runningFromVisualStudioDesigner;

    /// <summary> <see langword="True"/> if running from visual studio designer. </summary>
    /// <value> The running from visual studio designer. </value>
    public static bool RunningFromVisualStudioDesigner
    {
        get
        {
            if ( !_runningFromVisualStudioDesigner.HasValue )
            {
                using Process currentProcess = Process.GetCurrentProcess();
                _runningFromVisualStudioDesigner = _designerProcessNames.Contains( currentProcess.ProcessName.Trim(), StringComparer.OrdinalIgnoreCase );
            }

            return _runningFromVisualStudioDesigner.Value;
        }
    }

    #endregion

    #region " check 64 bits "

    /// <summary> Query if 'process' is 64 bit. </summary>
    /// <remarks>
    /// see https://msdn.Microsoft.com/en-us/library/windows/desktop/ms684139%28v=vs.85%29.aspx.
    /// </remarks>
    /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    /// null. </exception>
    /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    /// <param name="process"> The process. </param>
    /// <returns> <c>true</c> if 64 bit; otherwise <c>false</c> </returns>
    public static bool Is64Bit( Process process )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( process, nameof( process ) );
#else
        if ( process is null )
        {
            throw new ArgumentNullException( nameof( process ) );
        }
#endif

        if ( !Environment.Is64BitOperatingSystem )
        {
            return false;
        }
        // if this method is not available in your version of .NET, use GetNativeSystemInfo via P/Invoke instead
        return !NativeMethods.IsWow64Process( process.Handle, out bool isWow64 ) ? throw new InvalidOperationException() : !isWow64;
    }

    /// <summary> Query if 'process' is 64 bit. </summary>
    /// <remarks>
    /// see https://msdn.Microsoft.com/en-us/library/windows/desktop/ms684139%28v=vs.85%29.aspx.
    /// </remarks>
    /// <returns> <c>true</c> if 64 bit; otherwise <c>false</c> </returns>
    public static bool Is64Bit()
    {
        return Is64Bit( Process.GetCurrentProcess() );
    }

    /// <summary> A native methods. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    private sealed class NativeMethods
    {
        /// <summary> Initializes a new instance of the <see cref="object" /> class. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        private NativeMethods()
        {
        }

        /// <summary> Is wow 64 process. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="process"> [In] a pointer to the process. </param>
        /// <param name="wow64Process"> [Out] True if WOW 64 process. </param>
        /// <returns> True if success </returns>
        [DllImport( "kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Interoperability", "SYSLIB1054:Use 'LibraryImportAttribute' instead of 'DllImportAttribute' to generate P/Invoke marshalling code at compile time", Justification = "<Pending>" )]
        internal static extern bool IsWow64Process( [In()] IntPtr process, [MarshalAs( UnmanagedType.Bool )] out bool wow64Process );
    }

    #endregion
}
