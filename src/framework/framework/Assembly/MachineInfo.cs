using System;
using System.Management;
using Microsoft.Win32;

#pragma warning disable IDE0079

namespace cc.isr.Win;

/// <summary> A sealed class designed to provide machine information for the assembly. </summary>
/// <remarks>
/// <para>Machine identification (c) 2016 ZEEV Goldstein
/// http://www.codeproject.com/Tips/1125745/Machine-Finger-Print-The-Right-and-Efficient-Way#_comments
/// </para>. (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2016-09-08 </para>
/// </remarks>
public sealed class MachineInfo
{
    #region " construction and cleanup "

    /// <summary> Initializes a new instance of the <see cref="MachineInfo" /> class. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    private MachineInfo() : base()
    { }

    #endregion

    #region " registery access "

    /// <summary> Reads a key from the registry. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <param name="hive">          The hive. </param>
    /// <param name="keyFolderName"> Pathname of the key folder. </param>
    /// <param name="keyName">       Name of the key. </param>
    /// <param name="defaultValue">  The default value. </param>
    /// <returns> The key. </returns>
    public static string? ReadRegistry( RegistryHive hive, string keyFolderName, string keyName, string defaultValue )
    {
        RegistryView rv = Environment.Is64BitOperatingSystem ? RegistryView.Registry64 : RegistryView.Registry32;
        object? readValue = null;
        using ( RegistryKey regKeyBase = RegistryKey.OpenBaseKey( hive, rv ) )
        {
#if NET8_0_OR_GREATER
            using RegistryKey? regKey = regKeyBase.OpenSubKey( keyFolderName, RegistryKeyPermissionCheck.ReadSubTree );
#else
            using RegistryKey? regKey = regKeyBase.OpenSubKey( keyFolderName, RegistryKeyPermissionCheck.ReadSubTree );
#endif
            readValue = regKey?.GetValue( keyName, defaultValue );
        }

        if ( readValue is not null )
        {
            defaultValue = readValue.ToString() ?? string.Empty;
        }

        return defaultValue;
    }

    #endregion

    #region " machine identification (c) 2016 zeev goldstein "

    /// <summary> Unique identifier for the windows. </summary>
    private static string _windowsGuid = string.Empty;

    /// <summary> Windows unique identifier. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <returns> A <see cref="string" />. </returns>
    public static string WindowsGuid()
    {
        try
        {
            if ( string.IsNullOrEmpty( _windowsGuid ) )
            {
                RegistryView rv = RegistryView.Registry32;
                rv = Environment.Is64BitOperatingSystem ? RegistryView.Registry64 : RegistryView.Registry32;
                object? readValue = null;
                string defaultValue = "defaultValue";
                using ( RegistryKey regKeyBase = RegistryKey.OpenBaseKey( RegistryHive.LocalMachine, rv ) )
                {
                    using RegistryKey? regKey = regKeyBase.OpenSubKey( @"SOFTWARE\Microsoft\Cryptography", RegistryKeyPermissionCheck.ReadSubTree );
                    readValue = regKey?.GetValue( "MachineGuid", defaultValue );
                }

                if ( readValue is not null && readValue.ToString() != "defaultValue" )
                {
                    _windowsGuid = readValue.ToString() ?? string.Empty;
                }
            }
        }
        catch ( Exception )
        {
            _windowsGuid = string.Empty;
        }

        return _windowsGuid;
    }

    /// <summary> The machine UUID. </summary>
    private static string _machineUUID = string.Empty;

    /// <summary> Machine UUID. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <returns> A <see cref="string" />. </returns>
    public static string MachineUUID()
    {
        try
        {
            if ( string.IsNullOrEmpty( _machineUUID ) )
            {
                using ManagementObjectSearcher searcher = new( @"root\CIMV2", "SELECT * FROM Win32_ComputerSystemProduct" );
                foreach ( ManagementBaseObject? queryObj in searcher.Get() )
                {
                    _machineUUID += ( string ) queryObj["UUID"];
                }
            }
        }
        catch ( ManagementException )
        {
        }

        return _machineUUID;
    }

    /// <summary> Builds machine unique identifier. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    /// <param name="preSalt">   The pre-salt. </param>
    /// <param name="postSalt">  The post salt. </param>
    /// <param name="algorithm"> The algorithm. </param>
    /// <returns> A <see cref="string" />. </returns>
    public static string BuildMachineUniqueId( string preSalt, string postSalt, System.Security.Cryptography.HashAlgorithm algorithm )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( algorithm, nameof( algorithm ) );
#else
        if ( algorithm is null )
        {
            throw new ArgumentNullException( nameof( algorithm ) );
        }
#endif

        System.Text.StringBuilder hashedUniqueId = new();
        string uniqueId = $"{preSalt}{WindowsGuid()}{MachineUUID()}{postSalt}";
        byte[] bytesToHash = System.Text.Encoding.ASCII.GetBytes( uniqueId );
        bytesToHash = algorithm.ComputeHash( bytesToHash );
        foreach ( byte b in bytesToHash )
        {
            _ = hashedUniqueId.Append( b.ToString( "x2", System.Globalization.CultureInfo.CurrentCulture ) );
        }

        return hashedUniqueId.ToString();
    }

    private static string _machineUniqueIdMD5 = string.Empty;

    /// <summary> Builds a machine unique identifier using insecure MD5 hash. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <returns> A <see cref="string" />. </returns>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Security", "CA5351:Do Not Use Broken Cryptographic Algorithms", Justification = "<Pending>" )]
    [Obsolete( "Do not use broken cryptographic algorithms." )]
    public static string BuildMachineUniqueIdMD5()
    {
        if ( string.IsNullOrWhiteSpace( _machineUniqueIdMD5 ) )
        {
            using System.Security.Cryptography.MD5 algorithm = System.Security.Cryptography.MD5.Create();
            _machineUniqueIdMD5 = BuildMachineUniqueId( "20", "16", algorithm );
        }

        return _machineUniqueIdMD5;
    }

    private static string _machineUniqueIdSha1 = string.Empty;

    /// <summary> Builds a machine unique identifier using SHA1. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <returns> A <see cref="string" />. </returns>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Security", "CA5350:Do Not Use Weak Cryptographic Algorithms", Justification = "<Pending>" )]
    [Obsolete( "Do not use weak cryptographic algorithms." )]
    public static string BuildMachineUniqueIdSha1()
    {
        if ( string.IsNullOrWhiteSpace( _machineUniqueIdSha1 ) )
        {
            using System.Security.Cryptography.SHA1 algorithm = System.Security.Cryptography.SHA1.Create();
            _machineUniqueIdSha1 = BuildMachineUniqueId( "20", "16", algorithm );
        }

        return _machineUniqueIdSha1;
    }

    private static string _machineUniqueIdSha256 = string.Empty;

    /// <summary> Builds a machine unique identifier using SHA256. </summary>
    /// <remarks> David, 2020-09-15. </remarks>
    /// <returns> A <see cref="string" />. </returns>
    public static string BuildMachineUniqueIdSha256()
    {
        if ( string.IsNullOrWhiteSpace( _machineUniqueIdSha256 ) )
        {
            using System.Security.Cryptography.SHA256 algorithm = System.Security.Cryptography.SHA256.Create();
            _machineUniqueIdSha256 = BuildMachineUniqueId( "20", "16", algorithm );
        }

        return _machineUniqueIdSha256;
    }

    #endregion
}
