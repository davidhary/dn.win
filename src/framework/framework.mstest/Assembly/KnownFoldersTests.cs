using cc.isr.Std.Tests.Extensions;
using cc.isr.Std.Tests;

namespace cc.isr.Win.Framework.MSTest;
/// <summary>
/// This is a test class for KnownFoldersTest and is intended
/// to contain all KnownFoldersTest Unit Tests
/// </summary>
[TestClass]
public class KnownFoldersTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string methodFullName = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType?.Name}";
            if ( Logger is null )
                Console.WriteLine( methodFullName );
            else
                Logger?.LogInformationMultiLineMessage( methodFullName );
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets the test context which provides information about and functionality for the current test
    /// run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    /// <summary>   Gets a logger instance for this category. </summary>
    /// <value> The logger. </value>
    public static ILogger<KnownFoldersTests>? Logger { get; } = LoggerProvider.CreateLogger<KnownFoldersTests>();

    #endregion

    /// <summary> A test for GetPath. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="knownFolder"> A known folder enum value. </param>
    /// <param name="expected">    The expected value. </param>
    public static void TestGetPath( KnownFolder knownFolder, string expected )
    {
        string? actual = KnownFolders.GetPath( knownFolder );
        Assert.AreEqual( expected, actual, true, System.Globalization.CultureInfo.CurrentCulture );
    }

    /// <summary> A test for Get Path on multiple <see cref="KnownFolder"/> folders. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void GetPathTest()
    {
        TestGetPath( KnownFolder.Documents, @$"C:\Users\{Environment.UserName}\Documents" );
        TestGetPath( KnownFolder.UserProfiles, @"C:\Users" );
        TestGetPath( KnownFolder.ProgramData, @"C:\ProgramData" );
    }

    /// <summary> A test for Get Default Path. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    /// <param name="knownFolder"> A known folder enum value. </param>
    /// <param name="expected">    The expected value. </param>
    public static void TestGetDefaultPath( KnownFolder knownFolder, string expected )
    {
        string? actual = KnownFolders.GetDefaultPath( knownFolder );
        Assert.AreEqual( expected, actual, true, System.Globalization.CultureInfo.CurrentCulture );
    }

    /// <summary> A test for GetDefaultPath. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod()]
    public void GetDefaultPathTest()
    {
        TestGetDefaultPath( KnownFolder.Documents, @$"C:\Users\{Environment.UserName}\Documents" );
        TestGetDefaultPath( KnownFolder.UserProfiles, @"C:\Users" );
        TestGetDefaultPath( KnownFolder.ProgramData, @"C:\ProgramData" );
    }
}
