using cc.isr.Std.Tests;
using cc.isr.Std.Tests.Extensions;

namespace cc.isr.Win.Framework.MSTest;

/// <summary>   (Unit Test Class) a high resolution time tests. </summary>
/// <remarks>
/// <para>
/// David, 2018-03-14 </para>
/// </remarks>
[TestClass]
public class HighResolutionTimeTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string methodFullName = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType?.Name}";
            if ( Logger is null )
                Console.WriteLine( methodFullName );
            else
                Logger?.LogInformationMultiLineMessage( methodFullName );
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets the test context which provides information about and functionality for the current test
    /// run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    /// <summary>   Gets a logger instance for this category. </summary>
    /// <value> The logger. </value>
    public static ILogger<HighResolutionTimeTests>? Logger { get; } = LoggerProvider.CreateLogger<HighResolutionTimeTests>();


    #endregion

    #region " precise date time "

    /// <summary>   (Unit Test Method) tests high resolution date time resolution. </summary>
    /// <remarks>   David, 2020-12-07. </remarks>
    [TestMethod()]
    public void HighResolutionDateTimeResolutionTest()
    {
        string output = "";
        double sum = 0;
        double resolution;
        long ticks;
        long newTicks;
        int count = 20;
        for ( int ctr = 0; ctr <= count; ctr++ )
        {
            output += string.Format( System.Globalization.CultureInfo.CurrentCulture, $"{Win32.HighResolutionDateTime.UtcNow.ToFileTime()}\n" );

            // Introduce a delay loop.
            ticks = Win32.HighResolutionDateTime.UtcNow.ToFileTime();
            do
            { newTicks = Win32.HighResolutionDateTime.UtcNow.ToFileTime(); }
            while ( ticks == newTicks );
            resolution = 0.0001 * (newTicks - ticks);
            sum += resolution;
            output += string.Format( System.Globalization.CultureInfo.CurrentCulture, $"Millisecond resolution = {resolution}\n" );

            if ( ctr == 10 )
            {
                output += "Thread.Sleep called...\n";
                System.Threading.Thread.Sleep( 5 );
            }

        }
        resolution = sum / count;
        output += string.Format( System.Globalization.CultureInfo.CurrentCulture, $"Average resolution = {resolution}\n" );
        Trace.TraceInformation( output );
        double expectedResolution = 2 * cc.isr.Std.TimeSpanExtensions.TimeSpanExtensionMethods.MillisecondsPerTick;
        double resolutionEpsilon = 0.5 * expectedResolution;
        Assert.AreEqual( expectedResolution, resolution, resolutionEpsilon, $"Date time resolution {resolution} ms should match within {resolutionEpsilon} ms" );
    }

    #endregion
}
