namespace cc.isr.Win.Framework.MSTest;

/// <summary> An environment helper tests. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-10-16 </para>
/// </remarks>
[TestClass]
public class EnvHelperTests
{
    /// <summary> The variable not present key. </summary>
    private const string VARIBALE_NOT_PRESENT_KEY = "_VariableNotPresentKey";

    /// <summary> (Unit Test Method) tests retrieve string value. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod]
    public void RetrieveStringValueTest()
    {
        string key = "A_STRING";
        string value = "This is a string";
        Environment.SetEnvironmentVariable( key, value );
        Assert.AreEqual( value, Env.RetrieveValue( key ) );
        Assert.AreEqual( null, Env.RetrieveValue( VARIBALE_NOT_PRESENT_KEY ) );
        Assert.AreEqual( "none", Env.RetrieveValue( VARIBALE_NOT_PRESENT_KEY, "none" ) );
    }

    /// <summary> (Unit Test Method) tests retrieve boolean value. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod]
    public void RetrieveBooleanValueTest()
    {
        string key1 = "TRUE_VALUE";
        string value1 = "true";
        string key2 = "FALSE_VALUE";
        string value2 = "false";
        Environment.SetEnvironmentVariable( key1, value1 );
        Environment.SetEnvironmentVariable( key2, value2 );
        Assert.AreEqual( true, Env.RetrieveBoolean( key1 ).GetValueOrDefault( false ), $"{key1} should be {value1}" );
        Assert.AreEqual( false, ( object? ) Env.RetrieveBoolean( key2 ), $"{key2} should be {value2}" );
        Assert.AreEqual( default, Env.RetrieveBoolean( VARIBALE_NOT_PRESENT_KEY ), $"{VARIBALE_NOT_PRESENT_KEY} should be missing and default to {null}" );
        Assert.AreEqual( true, ( object? ) Env.RetrieveBoolean( VARIBALE_NOT_PRESENT_KEY, true ), $"{VARIBALE_NOT_PRESENT_KEY} should be missing and was set to default {true}" );
    }

    /// <summary> (Unit Test Method) tests retrieve integer value. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod]
    public void RetrieveIntegerValueTest()
    {
        string key1 = "ONE_STRING";
        string value1 = "1";
        Environment.SetEnvironmentVariable( key1, value1 );
        Assert.AreEqual( 1, ( object? ) Env.RetrieveInteger( key1 ) );
        Assert.AreEqual( default, Env.RetrieveInteger( VARIBALE_NOT_PRESENT_KEY ) );
        Assert.AreEqual( -1, ( object? ) Env.RetrieveInteger( VARIBALE_NOT_PRESENT_KEY, -1 ) );
    }

    /// <summary> (Unit Test Method) tests retrieve double value. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestMethod]
    public void RetrieveDoubleValueTest()
    {
        string key1 = "ONE_POINT_TWO_STRING";
        string value1 = "1.2";
        string key2 = "ONE_POINT_TWO_STRING_WITH_COMMA";
        string value2 = "1,2";
        Environment.SetEnvironmentVariable( key1, value1 );
        Environment.SetEnvironmentVariable( key2, value2 );
        Assert.AreEqual( 1.2d, ( object? ) Env.RetrieveDouble( key1 ) );
        Assert.AreEqual( 12.0d, ( object? ) Env.RetrieveDouble( key2 ) );
        Assert.AreEqual( default, Env.RetrieveDouble( VARIBALE_NOT_PRESENT_KEY ) );
        Assert.AreEqual( -1.2d, ( object? ) Env.RetrieveDouble( VARIBALE_NOT_PRESENT_KEY, -1.2d ) );
    }
}
