using cc.isr.Std.Tests;
using cc.isr.Std.Tests.Extensions;

namespace cc.isr.Win.Framework.Trials;

/// <summary> A machine information tests. </summary>
/// <remarks> David, 2020-09-23. </remarks>
[TestClass]
public class MachineInfoTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string methodFullName = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType?.Name}";
            if ( Logger is null )
                Console.WriteLine( methodFullName );
            else
                Logger?.LogInformationMultiLineMessage( methodFullName );
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
            _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        System.Diagnostics.Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {System.TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static Tracing.TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets the test context which provides information about and functionality for the current test
    /// run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    /// <summary>   Gets a logger instance for this category. </summary>
    /// <value> The logger. </value>
    public static ILogger<MachineInfoTests>? Logger { get; } = LoggerProvider.CreateLogger<MachineInfoTests>();

    #endregion

    /// <summary> A test for Machine ID Using insecure MD5 algorithm,. </summary>
    /// <remarks> Causes application domain exception with test agent. </remarks>
    [TestMethod()]
    [Obsolete( "no longer using MD5" )]
    public void MachineUniqueIdMD5Test()
    {
        string expected = "6f32a7c3d4b883fb67303c53380b7859";
        string computerName = System.Environment.MachineName;
        switch ( true )
        {
            case object when string.Equals( computerName, "LimeDev", StringComparison.OrdinalIgnoreCase ):
                {
                    expected = "f77145622be6795484b53c570731a5fc";
                    break;
                }

            case object when string.Equals( computerName, "Fig10Dev", StringComparison.OrdinalIgnoreCase ):
                {
                    expected = "7ae637a47a35f672ed013462c7b92649";
                    break;
                }

            case object when string.Equals( computerName, "LimeDevB", StringComparison.OrdinalIgnoreCase ):
                {
                    expected = "6f32a7c3d4b883fb67303c53380b7859";
                    break;
                }

            default:
                break;
        }
        // on FIG10Dev
        string actual = MachineInfo.BuildMachineUniqueIdMD5();
        Assert.AreEqual( expected, actual );
    }

    /// <summary> (Unit Test Method) machine unique identifier SHA 1. </summary>
    /// <remarks> Causes application domain exception with test agent. </remarks>
    [TestMethod()]
    [Obsolete( "use sha256" )]
    public void MachineUniqueIdSha1()
    {
        string expected = "be9be0ce1a031b079b0330be7e47f0dbcba2a1cd";
        string computerName = System.Environment.MachineName;
        switch ( true )
        {
            case object when string.Equals( computerName, "LimeDev", StringComparison.OrdinalIgnoreCase ):
                {
                    expected = "e8744184d701a53a060f1502d40e2a8e09a61a31";
                    break;
                }

            case object when string.Equals( computerName, "Fig10Dev", StringComparison.OrdinalIgnoreCase ):
                {
                    expected = "85f9c966b0a3f128c41186a0160aa0c9cce68e65";
                    break;
                }

            case object when string.Equals( computerName, "LimeDevB", StringComparison.OrdinalIgnoreCase ):
                {
                    expected = "be9be0ce1a031b079b0330be7e47f0dbcba2a1cd";
                    break;
                }

            default:
                break;
        }

        string actual = MachineInfo.BuildMachineUniqueIdSha1();
        Assert.AreEqual( expected, actual );
    }

    /// <summary> (Unit Test Method) machine unique identifier SHA 256. </summary>
    /// <remarks> Causes application domain exception with test agent. </remarks>
    [TestMethod()]
    public void MachineUniqueIdSha256()
    {
        string expected = "be9be0ce1a031b079b0330be7e47f0dbcba2a1cd";
        string computerName = System.Environment.MachineName;
        switch ( true )
        {
            case object when string.Equals( computerName, "LimeDev", StringComparison.OrdinalIgnoreCase ):
                {
                    expected = "e8744184d701a53a060f1502d40e2a8e09a61a31";
                    break;
                }

            case object when string.Equals( computerName, "Fig10Dev", StringComparison.OrdinalIgnoreCase ):
                {
                    expected = "85f9c966b0a3f128c41186a0160aa0c9cce68e65";
                    break;
                }

            case object when string.Equals( computerName, "LimeDevB", StringComparison.OrdinalIgnoreCase ):
                {
                    expected = "b265637e13e784924815631c366443d2663821e43189ee3b98b5d74350a93e10";
                    break;
                }

            default:
                break;
        }

        string actual = MachineInfo.BuildMachineUniqueIdSha256();
        Assert.AreEqual( expected, actual );
    }

}
