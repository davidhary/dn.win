using System;
using System.Collections;
using System.DirectoryServices.AccountManagement;
using System.Net;

namespace cc.isr.Win.Accounts;

/// <summary> Machine login. </summary>
/// <remarks>
/// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-10-06, 2.1.5392. </para>
/// </remarks>
public class MachineLogin : LoginBase
{
    /// <summary>   Authenticates the given user credential. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userCredential">   The user credential. </param>
    /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    public override bool Authenticate( NetworkCredential userCredential )
    {
        bool result = false;
        if ( userCredential is null )
        {
            throw new ArgumentNullException( nameof( userCredential ) );
        }
        else if ( string.IsNullOrWhiteSpace( userCredential.UserName ) )
        {
            this.ValidationMessage = $"{nameof( NetworkCredential.UserName )} is empty";
        }
        else if ( string.IsNullOrWhiteSpace( userCredential.Password ) )
        {
            this.ValidationMessage = $"{nameof( NetworkCredential.Password )}  is empty";
        }
        else
        {
            try
            {
                using PrincipalContext ctx = new( ContextType.Machine );
                using UserPrincipal? user = UserPrincipal.FindByIdentity( ctx, userCredential.UserName );
                if ( user is null )
                {
                    this.ValidationMessage = $"User '{userCredential.UserName}' not identified on this machine";
                }
                else
                {
                    this.UserRoles = DomainLogin.EnumerateUserRoles( user );
                    result = ctx.ValidateCredentials( userCredential.UserName, userCredential.Password );
                    if ( result )
                    {
                        this.UserName = userCredential.UserName;
                    }
                    else
                    {
                        this.UserName = string.Empty;
                        this.ValidationMessage = $"Failed validating credentials for user '{userCredential.UserName}'";
                    }
                }
            }
            catch ( Exception ex )
            {
                this.ValidationMessage = ex.ToString();
            }
        }

        this.IsAuthenticated = result;
        this.Failed = !result;
        return result;
    }

    /// <summary> Tries to find a user in a group role. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="userName">         Specifies a user name. </param>
    /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    /// the name of enumeration flags. </param>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    public override bool TryFindUser( string userName, ArrayList allowedUserRoles )
    {
        bool result = false;
        this.Failed = false;
        if ( string.IsNullOrWhiteSpace( userName ) )
        {
            this.ValidationMessage = $"{nameof( userName )} is empty";
            this.Failed = true;
        }
        else if ( allowedUserRoles is null )
        {
            this.ValidationMessage = $"{nameof( allowedUserRoles )} are not specified";
            this.Failed = true;
        }
        else if ( allowedUserRoles.Count == 0 )
        {
            this.ValidationMessage = $"{nameof( allowedUserRoles )} not set";
            this.Failed = true;
        }
        else
        {
            try
            {
                using PrincipalContext ctx = new( ContextType.Machine );
                using UserPrincipal? user = UserPrincipal.FindByIdentity( ctx, userName );
                if ( user is null )
                {
                    this.ValidationMessage = $"User '{userName}' not identified on this machine";
                }
                else
                {
                    this.UserRoles = DomainLogin.EnumerateUserRoles( user );
                    if ( DomainLogin.EnumerateUserRoles( user, allowedUserRoles ).Count > 0 )
                    {
                        this.UserName = userName;
                        result = true;
                    }

                    if ( !result )
                    {
                        this.ValidationMessage = $"User '{userName}' role was not found among the allowed user roles.";
                    }
                }
            }
            catch ( Exception ex )
            {
                this.ValidationMessage = ex.ToString();
            }
        }

        return result;
    }

    #region " manager users "

    /// <summary>   Adds a user to group to 'groupName'. </summary>
    /// <remarks>   David, 2021-05-17. </remarks>
    /// <param name="userId">       Identifier for the user. </param>
    /// <param name="groupName">    Name of the group. </param>
    public static void AddUserToGroup( string userId, string groupName )
    {
        using PrincipalContext pc = new( ContextType.Machine );
        LoginBase.AddUserToGroup( pc, userId, groupName );
    }

    /// <summary>   Removes the user from group. </summary>
    /// <remarks>   David, 2021-05-17. </remarks>
    /// <param name="userId">       Identifier for the user. </param>
    /// <param name="groupName">    Name of the group. </param>
    public static void RemoveUserFromGroup( string userId, string groupName )
    {
        using PrincipalContext pc = new( ContextType.Machine );
        LoginBase.RemoveUserFromGroup( pc, userId, groupName );
    }

    /// <summary>   Creates a user. </summary>
    /// <remarks>   David, 2021-05-17. </remarks>
    /// <param name="userId">           Identifier for the user. </param>
    /// <param name="password">         The password. </param>
    /// <param name="userDescription">  (Optional) Information describing the user. </param>
    /// <returns>   The new user. </returns>
    public static UserPrincipal CreateUser( string userId, string password, string userDescription = "Created from .NET" )
    {
        using PrincipalContext pc = new( ContextType.Machine );
        return LoginBase.CreateUser( pc, userId, password, userDescription );
    }

    /// <summary>   Creates a user. </summary>
    /// <remarks>   David, 2021-05-17. </remarks>
    /// <param name="userId">           Identifier for the user. </param>
    /// <param name="password">         The password. </param>
    /// <param name="userDescription">  (Optional) Information describing the user. </param>
    /// <param name="groupName">        (Optional) Name of the group. </param>
    public static void CreateUser( string userId, string password, string userDescription = "Created from .NET", string groupName = "Guests" )
    {
        _ = CreateUser( userId, password, userDescription );
        AddUserToGroup( userId, groupName );
    }

    #endregion

    #region "  " authenticate "  "

    /// <summary>   Try authenticate. </summary>
    /// <remarks>   David, 2021-05-17. </remarks>
    /// <param name="userCredential">   The user credential. </param>
    /// <param name="requiredGroup">    the group to which the user must belong. </param>
    /// <returns>   A Tuple. </returns>
    public static (bool Success, string Details) TryAuthenticate( NetworkCredential userCredential, string requiredGroup )
    {
        using PrincipalContext ctx = new( ContextType.Machine );
        return LoginBase.TryAuthenticate( ctx, userCredential, requiredGroup );
    }

    /// <summary>   Try authenticate. </summary>
    /// <remarks>   David, 2021-05-13. </remarks>
    /// <param name="userId">           Identifier for the user. </param>
    /// <param name="password">         Specifies a password. </param>
    /// <param name="requiredGroup">    the group to which the user must belong. </param>
    /// <returns>   A Tuple. </returns>
    public static (bool Success, string Details) TryAuthenticate( string userId, string password, string requiredGroup )
    {
        using PrincipalContext ctx = new( ContextType.Machine );
        return LoginBase.TryAuthenticate( ctx, userId, password, requiredGroup );
    }

    #endregion
}
