using System;
using System.Collections;
using System.DirectoryServices.AccountManagement;
using System.Net;

namespace cc.isr.Win.Accounts;

/// <summary> Domain login. </summary>
/// <remarks>
/// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2014-10-10, 2.1.5396. </para>
/// </remarks>
public class DomainLogin : LoginBase
{
    #region " manage users "

    /// <summary>   Attempts to find user on the machine. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userName"> Specifies a user name. </param>
    /// <returns>   A tuple including the found user. </returns>
    public static (bool Success, string Details, UserPrincipal? User) FindUser( string userName )
    {
        UserPrincipal? user = null;
        (bool Success, string Details, UserPrincipal? User) result = (false, string.Empty, user);
        if ( string.IsNullOrWhiteSpace( userName ) )
        {
            result = (false, $"{nameof( userName )} is empty", user);
        }
        else
        {
            PrincipalContext? ctx = null;
            try
            {
                ctx = new PrincipalContext( ContextType.Machine );
            }
            catch ( Exception ex )
            {
                ctx?.Dispose();
                ctx = null;
                result = (false, $"Exception occurred;. {ex}", user);
            }

            if ( ctx is not null )
            {
                user = UserPrincipal.FindByIdentity( ctx, userName );
                result = (user is null)
                         ? (false, $"user1 '{userName}' not found @'{Environment.MachineName}'", user)
                         : (true, string.Empty, user);
            }
        }

        return result;
    }

    /// <summary>   Attempts to find user with the allowed roles on the machine. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userName">         Specifies a user name. </param>
    /// <param name="allowedUserRoles"> The allowed roles. </param>
    /// <returns>   A tuple including the found user. </returns>
    public static (bool Success, string Details, UserPrincipal? User) FindUser( string userName, ArrayList allowedUserRoles )
    {
        (bool Success, string Details, UserPrincipal? User) result = (false, string.Empty, null);
        if ( string.IsNullOrWhiteSpace( userName ) )
        {
            result = (false, $"{nameof( userName )} is empty", null);
        }
        else
        {
            try
            {
                result = FindUser( userName );
                if ( result.User is null )
                {
                }
                else if ( EnumerateUserRoles( result.User, allowedUserRoles ).Count == 0 )
                {
                    result.User?.Dispose();
                    result.User = null;
                    result = (false, $"user1 '{userName}' found @'{Environment.MachineName}' is not a member in any of the allowed groups. ", null);
                }
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception occurred;. {ex}", result.User);
            }
        }

        return result;
    }

    /// <summary>   Attempts to find user on the machine. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userName"> Specifies a user name. </param>
    /// <returns>   A Tuple. </returns>
    public static (bool Success, string Details) TryFindUser( string userName )
    {
        (bool Success, string Details) result;
        if ( string.IsNullOrWhiteSpace( userName ) )
        {
            result = (false, $"{nameof( userName )} is empty");
        }
        else
        {
            try
            {
                (bool success1, string details1, UserPrincipal? user1) = FindUser( userName );
                result = user1 is null
                    ? (false, $"user1 '{userName}' not found @'{Environment.MachineName}'")
                    : (success1, details1);
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception occurred;. {ex}");
            }
        }

        return result;
    }

    /// <summary>   Attempts to find user on the domain. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userName">     Specifies a user name. </param>
    /// <param name="domainName">   Name of the domain. </param>
    /// <returns>   A tuple including the found user. </returns>
    public static (bool Success, string Details, UserPrincipal? User) FindUser( string userName, string domainName )
    {
        (bool Success, string Details, UserPrincipal? User) result = (false, string.Empty, null);

        if ( string.IsNullOrWhiteSpace( userName ) )
        {
            result = (false, $"{nameof( userName )} is empty", null);
        }
        else if ( string.IsNullOrWhiteSpace( domainName ) )
        {
            result = (false, $"{nameof( domainName )} is empty", null);
        }
        else
        {
            PrincipalContext? ctx = null;
            try
            {
                ctx = new PrincipalContext( ContextType.Domain, domainName );
                result.User = UserPrincipal.FindByIdentity( ctx, userName );
                if ( result.User is null )
                {
                    result = (false, $"user1 '{userName}' not identified @'{domainName}'", null);
                }
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception occurred;. {ex}", null);
            }
            finally
            {
                if ( result.User is null )
                {
                    ctx?.Dispose();
                }
            }
        }

        return result;
    }

    /// <summary>   Attempts to find user on the domain with an allowed role. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userName">         Specifies a user name. </param>
    /// <param name="domainName">       Name of the domain. </param>
    /// <param name="allowedUserRoles"> The allowed roles. </param>
    /// <returns>   A tuple including the found user. </returns>
    public static (bool Success, string Details, UserPrincipal? User) FindUser( string userName, string domainName, ArrayList allowedUserRoles )
    {
        (bool Success, string Details, UserPrincipal? User) result = (false, string.Empty, null);
        if ( string.IsNullOrWhiteSpace( userName ) )
        {
            result = (false, $"{nameof( userName )} is empty", null);
        }
        else if ( string.IsNullOrWhiteSpace( domainName ) )
        {
            result = (false, $"{nameof( domainName )} is empty", null);
        }
        else
        {
            PrincipalContext? ctx = null;
            try
            {
                result = FindUser( userName, domainName );
                if ( result.User is null )
                {
                }
                else if ( EnumerateUserRoles( result.User, allowedUserRoles ).Count == 0 )
                {
                    result.User?.Dispose();
                    result.User = null;
                    result = (false, $"user1 '{userName}' found @'{domainName}' is not a member in any of the allowed groups.", null);
                }

                ctx = new PrincipalContext( ContextType.Domain, domainName );
                result.User = UserPrincipal.FindByIdentity( ctx, userName );
                if ( result.User is null )
                {
                    result = (false, $"user1 '{userName}' not identified @'{domainName}'", null);
                }
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception occurred;. {ex}", null);
            }
            finally
            {
                if ( result.User is null )
                {
                    ctx?.Dispose();
                }
            }
        }

        return result;
    }

    /// <summary>   Attempts to find user on the machine. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userName">     Specifies a user name. </param>
    /// <param name="domainName">   [in,out] Name of the domain. </param>
    /// <returns>   A tuple including the found user. </returns>
    public static (bool Success, string Details, UserPrincipal? User) FindNextUser( string userName, ref string domainName )
    {
        (bool Success, string Details, UserPrincipal? User) result = (false, string.Empty, null);
        if ( string.IsNullOrWhiteSpace( userName ) )
        {
            result = (false, $"{nameof( userName )} is empty", null);
        }
        else
        {
            try
            {
                ArrayList? domainNames = EnumerateDomains();
                if ( domainNames is null || domainNames.Count == 0 )
                {
                    result = (false, "No domains found; Most likely, the current security context is not associated with an Active Directory domain or forest.", null);
                }
                else
                {
                    bool foundDomain = string.IsNullOrEmpty( domainName );
                    foreach ( string name in domainNames )
                    {
                        if ( foundDomain )
                        {
                            // skip until the domain is found or empty.
                            result = FindUser( userName, name );
                            if ( result.User is not null )
                            {
                                domainName = name;
                                break;
                            }
                        }
                        else
                        {
                            foundDomain = string.Equals( name, domainName, StringComparison.OrdinalIgnoreCase );
                        }
                    }
                }
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception occurred;. {ex}", null);
            }
        }

        return result;
    }

    /// <summary>   Attempts to find user on the machine. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userName">         Specifies a user name. </param>
    /// <param name="allowedUserRoles"> The allowed roles. </param>
    /// <param name="domainName">       [in,out] Name of the domain. </param>
    /// <returns>   A tuple including the found user. </returns>
    public static (bool Success, string Details, UserPrincipal? User) FindNextUser( string userName, ArrayList allowedUserRoles, ref string domainName )
    {
        (bool Success, string Details, UserPrincipal? User) result = (true, string.Empty, null);
        if ( string.IsNullOrWhiteSpace( userName ) )
        {
            result = (false, $"{nameof( userName )} is empty", null);
        }
        else
        {
            try
            {
                ArrayList? domainNames = EnumerateDomains();
                if ( domainNames is null || domainNames.Count == 0 )
                {
                    result = (false, "No domains found; Most likely, the current security context is not associated with an Active Directory domain or forest.", null);
                }
                else
                {
                    bool foundDomain = string.IsNullOrEmpty( domainName );
                    foreach ( string name in domainNames )
                    {
                        if ( foundDomain )
                        {
                            // skip until the domain is found or empty.
                            result = FindUser( userName, name, allowedUserRoles );
                            if ( result.User is not null )
                            {
                                domainName = name;
                                break;
                            }
                        }
                        else
                        {
                            foundDomain = string.Equals( name, domainName, StringComparison.OrdinalIgnoreCase );
                        }
                    }
                }
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception occurred;. {ex}", null);
            }
        }

        return result;
    }

    /// <summary>   Attempts to find user on the machine. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userName">     Specifies a user name. </param>
    /// <param name="domainName">   Name of the domain. </param>
    /// <returns>   A Tuple. </returns>
    public static (bool Success, string Details) TryFindUser( string userName, string domainName )
    {
        (bool Success, string Details) result = (false, string.Empty);
        if ( string.IsNullOrWhiteSpace( userName ) )
        {
            result = (false, $"{nameof( userName )} is empty");
        }
        else
        {
            try
            {
                using PrincipalContext ctx = new( ContextType.Domain, domainName );
                using UserPrincipal? user = UserPrincipal.FindByIdentity( ctx, userName );
                result = user is null ? (false, $"user1 '{userName}' not found @{domainName}") : (true, string.Empty);
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception occurred;. {ex}");
            }
        }

        return result;
    }

    /// <summary>   Attempts to find user on the machine. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userName">     Specifies a user name. </param>
    /// <param name="contextType">  [in,out] Type of the context. </param>
    /// <returns>   A Tuple. </returns>
    public static (bool Success, string Details) TryFindUser( string userName, ref ContextType contextType )
    {
        // first try the machine.
        (bool Success, string Details) result = TryFindUser( userName );
        if ( result.Success )
        {
            contextType = ContextType.Machine;
        }
        else
        {
            ArrayList? domainNames = EnumerateDomains();
            if ( domainNames is null || domainNames.Count == 0 )
            {
                result = (false, "No domains found; Most likely, the current security context is not associated with an Active Directory domain or forest.");
            }
            else
            {
                contextType = ContextType.Domain;
                foreach ( string name in domainNames )
                {
                    result = TryFindUser( userName, name );
                    if ( result.Success )
                    {
                        break;
                    }
                }
            }
        }

        return result;
    }

    #endregion

    #region " authenticate "

    /// <summary>
    /// Authenticates a user name and password on the machine and then on the domain.
    /// </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userCredential">   The user credential. </param>
    /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    public override bool Authenticate( NetworkCredential userCredential )
    {
        (bool Success, string Details, UserPrincipal? User) result;
        if ( userCredential is null )
        {
            throw new ArgumentNullException( nameof( userCredential ) );
        }
        else if ( string.IsNullOrWhiteSpace( userCredential.UserName ) )
        {
            result = (false, $"{nameof( NetworkCredential.UserName )} is empty", null);
        }
        else if ( string.IsNullOrWhiteSpace( userCredential.Password ) )
        {
            result = (false, $"{nameof( NetworkCredential.Password )}  is empty", null);
        }
        else
        {
            try
            {
                result = FindUser( userCredential.UserName );
                this.ValidationMessage = result.Details;
                result = (result.User is not null && result.User.Context.ValidateCredentials( userCredential.UserName, userCredential.Password ), string.Empty, result.User);
                if ( !result.Success && result.User is not null )
                {
                    result.Details = $"user1 '{userCredential.UserName}' not authenticated @'{Environment.MachineName}";
                }

                if ( !result.Success )
                {
                    try
                    {
                        string domainName = string.Empty;
                        do
                        {
                            result = FindNextUser( userCredential.UserName, ref domainName );
                            this.ValidationMessage = result.Details;
                            result.Success = result.Success && result.User is not null && result.User.Context.ValidateCredentials( userCredential.UserName, userCredential.Password );
                            if ( !result.Success && result.User is not null )
                            {
                                result.Details = $"user1 '{userCredential.UserName}' not authenticated @'{domainName}";
                            }
                        }
                        while ( !result.Success && result.User is not null );
                        if ( result.Success )
                        {
                            this.UserRoles = EnumerateUserRoles( result.User! );
                            this.UserName = userCredential.UserName;
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        result.User?.Dispose();
                        result.User = null;
                    }
                }
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception validating credentials; {ex}", null);
            }
        }

        this.ValidationMessage = result.Details;
        this.IsAuthenticated = result.Success;
        this.Failed = !result.Success;
        return result.Success;
    }

    /// <summary>   Authenticates a user name and password. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="userCredential">   The user credential. </param>
    /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    ///                                 the name of enumeration flags. </param>
    /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    public override bool Authenticate( NetworkCredential userCredential, ArrayList allowedUserRoles )
    {
        (bool Success, string Details, UserPrincipal? User) result;
        if ( userCredential is null )
        {
            throw new ArgumentNullException( nameof( userCredential ) );
        }
        else if ( string.IsNullOrWhiteSpace( userCredential.UserName ) )
        {
            result = (false, $"{nameof( NetworkCredential.UserName )} is empty", null);
        }
        else if ( string.IsNullOrWhiteSpace( userCredential.Password ) )
        {
            result = (false, $"{nameof( NetworkCredential.Password )}  is empty", null);
        }
        else if ( allowedUserRoles is null )
        {
            result = (false, $"{nameof( allowedUserRoles )} are not specified", null);
        }
        else if ( allowedUserRoles.Count == 0 )
        {
            result = (false, $"{nameof( allowedUserRoles )} not set", null);
        }
        else
        {
            try
            {
                result = FindUser( userCredential.UserName, allowedUserRoles );
                {
                    result.Success = result.Success && result.User is not null && result.User.Context.ValidateCredentials( userCredential.UserName, userCredential.Password );
                    if ( !result.Success && result.User is not null )
                    {
                        result.Details = $"user1 '{userCredential.UserName}' not authenticated @'{Environment.MachineName}";
                    }
                }

                if ( !result.Success )
                {
                    try
                    {
                        string domainName = string.Empty;
                        do
                        {
                            result = FindNextUser( userCredential.UserName, allowedUserRoles, ref domainName );
                            result.Success = result.Success && result.User is not null && result.User.Context.ValidateCredentials( userCredential.UserName, userCredential.Password );
                            if ( !result.Success && result.User is not null )
                            {
                                result.Details = $"user1 '{userCredential.UserName}' not authenticated @'{domainName}";
                            }
                        }
                        while ( !result.Success && result.User is not null );
                        if ( result.Success )
                        {
                            this.UserRoles = EnumerateUserRoles( result.User! );
                            this.UserName = userCredential.UserName;
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        result.User?.Dispose();
                        result.User = null;
                    }
                }
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception validating credentials; {ex}", null);
            }
        }

        this.ValidationMessage = result.Details;
        this.IsAuthenticated = result.Success;
        this.Failed = !result.Success;
        return result.Success;
    }

    /// <summary>   Tries to find a user in a group role. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userName">         Specifies a user name. </param>
    /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    ///                                 the name of enumeration flags. </param>
    /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    public override bool TryFindUser( string userName, ArrayList allowedUserRoles )
    {
        (bool Success, string Details, UserPrincipal? User) result;
        if ( string.IsNullOrWhiteSpace( userName ) )
        {
            result = (false, $"{nameof( userName )} is empty", null);
        }
        else if ( allowedUserRoles is null )
        {
            result = (false, $"{nameof( allowedUserRoles )} are not specified", null);
        }
        else if ( allowedUserRoles.Count == 0 )
        {
            result = (false, $"{nameof( allowedUserRoles )} not set", null);
        }
        else
        {
            try
            {
                result = FindUser( userName );
                {
                    result.Success = result.Success && result.User is not null && EnumerateUserRoles( result.User, allowedUserRoles ).Count > 0;
                    if ( !result.Success && result.User is not null )
                    {
                        result.Details = $"user1 '{userName}' is not a member in any of the allowed groups @'{Environment.MachineName}";
                    }
                }

                if ( !result.Success )
                {
                    try
                    {
                        string domainName = string.Empty;
                        do
                        {
                            result = FindNextUser( userName, ref domainName );
                            result.Success = result.Success && result.User is not null && EnumerateUserRoles( result.User, allowedUserRoles ).Count > 0;
                            if ( !result.Success && result.User is not null )
                            {
                                result.Details = $"user1 '{userName}' is not a member in any of the allowed groups @'{domainName}";
                            }
                        }
                        while ( !result.Success && result.User is not null );
                        if ( result.Success )
                        {
                            this.UserRoles = EnumerateUserRoles( result.User! );
                            if ( EnumerateUserRoles( result.User!, allowedUserRoles ).Count > 0 )
                            {
                                this.UserName = userName;
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        result.User?.Dispose();
                        result.User = null;
                    }
                }
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception validating credentials; {ex}", null);
            }
        }

        this.ValidationMessage = result.Details;
        this.IsAuthenticated = result.Success;
        this.Failed = !result.Success;
        return result.Success;
    }
    #endregion

    #region " manage users "

    /// <summary>   Adds a user to group to 'groupName'. </summary>
    /// <remarks>   David, 2021-05-17. </remarks>
    /// <param name="userId">       Identifier for the user. </param>
    /// <param name="groupName">    Name of the group. </param>
    public static void AddUserToGroup( string userId, string groupName )
    {
        using PrincipalContext pc = new( ContextType.Domain, "COMPANY" );
        LoginBase.AddUserToGroup( pc, userId, groupName );
    }

    /// <summary>   Removes the user from group. </summary>
    /// <remarks>   David, 2021-05-17. </remarks>
    /// <param name="userId">       Identifier for the user. </param>
    /// <param name="groupName">    Name of the group. </param>
    public static void RemoveUserFromGroup( string userId, string groupName )
    {
        using PrincipalContext pc = new( ContextType.Domain, "COMPANY" );
        LoginBase.RemoveUserFromGroup( pc, userId, groupName );
    }

    #endregion
}
