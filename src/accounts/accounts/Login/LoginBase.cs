using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.ActiveDirectory;
using System.Threading;

namespace cc.isr.Win.Accounts;

/// <summary> Interfaces user log in/out functionality. </summary>
/// <remarks>
/// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
/// Licensed under The MIT License.</para><para>
/// David, 2008-10-30, 1.00.3225 </para>
/// </remarks>
[System.Runtime.InteropServices.ComVisible( false )]
public abstract class LoginBase : IDisposable, INotifyPropertyChanged
{
    #region " construction and cleanup "

    /// <summary> Initializes a new instance of the <see cref="LoginBase" /> class. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    protected LoginBase() : base()
    { }

    /// <summary> Calls <see cref="Dispose(bool)" /> to cleanup. </summary>
    /// <remarks>
    /// Do not make this method Overridable (virtual) because a derived class should not be able to
    /// override this method.
    /// </remarks>
    public void Dispose()
    {
        // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        // this disposes all child classes.
        this.Dispose( true );

        // Take this object off the finalization(Queue) and prevent finalization code 
        // from executing a second time.
        GC.SuppressFinalize( this );
    }

    /// <summary>
    /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
    /// class provided proper implementation.
    /// </summary>
    /// <value> <c>true</c> if disposed; otherwise, <c>false</c>. </value>
    protected bool IsDisposed { get; set; }

    /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    /// <remarks>
    /// Executes in two distinct scenarios as determined by its disposing parameter:<para>
    /// If True, the method has been called directly or indirectly by a user's code--managed and
    /// unmanaged resources can be disposed.</para><para>
    /// If False, the method has been called by the runtime from inside the finalizer and you should
    /// not reference other objects--only unmanaged resources can be disposed.</para>
    /// </remarks>
    /// <param name="disposing"> <c>true</c> if this method releases both managed and unmanaged
    /// resources;
    /// False if this method releases only unmanaged
    /// resources. </param>
    protected virtual void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.RemovePropertyChangedEventHandlers();
            }
        }
        finally
        {
            // set the sentinel indicating that the class was disposed.
            this.IsDisposed = true;
        }
    }

    #endregion

    #region " synchronization context "

    /// <summary> Gets or sets the synchronization context. </summary>
    /// <value> The context. </value>
    private SynchronizationContext? Context { get; set; }

    /// <summary> Returns the current synchronization context. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is
    /// null. </exception>
    /// <returns> A Threading.SynchronizationContext. </returns>
    private static SynchronizationContext CurrentSyncContext()
    {
        if ( SynchronizationContext.Current is null )
        {
            SynchronizationContext.SetSynchronizationContext( new SynchronizationContext() );
        }

        return SynchronizationContext.Current is null
            ? throw new InvalidOperationException( "Current Synchronization Context not set;. Must be set before starting the thread." )
            : SynchronizationContext.Current;
    }

    private bool _usingAsyncNotification;

    /// <summary>   Gets or sets the notification level. </summary>
    /// <value> The notification level. </value>
    public bool UsingAsyncNotification
    {
        get => this._usingAsyncNotification;
        set
        {
            if ( !bool.Equals( value, this.UsingAsyncNotification ) )
            {
                this._usingAsyncNotification = value;
                this.NotifyPropertyChanged();
            }
        }
    }

    /// <summary> Gets a context for the active. </summary>
    /// <value> The active context. </value>
    protected SynchronizationContext ActiveContext => this.Context ?? CurrentSyncContext();

    #endregion

    #region " notify property change implementation "

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <param name="propertyName"> Name of the property. </param>
    protected virtual void OnPropertyChanged( string? propertyName )
    {
        if ( !string.IsNullOrEmpty( propertyName ) )
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="backingField"> [in,out] The backing field. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected virtual bool OnPropertyChanged<T>( ref T backingField, T value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = "" )
    {
        if ( EqualityComparer<T>.Default.Equals( backingField, value ) )
            return false;

        backingField = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="prop">         [in,out] The property. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<T>( ref T prop, T value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
        if ( EqualityComparer<T>.Default.Equals( prop, value ) ) return false;
        prop = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <remarks>   2023-03-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="oldValue">     The old value. </param>
    /// <param name="newValue">     The new value. </param>
    /// <param name="callback">     The callback. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<T>( T oldValue, T newValue, Action callback, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
        if ( callback is null ) throw new ArgumentNullException( nameof( callback ) );
#endif
#endif

        if ( EqualityComparer<T>.Default.Equals( oldValue, newValue ) )
        {
            return false;
        }

        callback();

        this.OnPropertyChanged( propertyName );

        return true;
    }

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Removes the property changed event handlers. </summary>
    /// <remarks>   David, 2021-06-28. </remarks>
    protected void RemovePropertyChangedEventHandlers()
    {
        PropertyChangedEventHandler? handler = this.PropertyChanged;
        if ( handler is not null )
        {
            foreach ( Delegate item in handler.GetInvocationList() )
            {
                handler -= ( PropertyChangedEventHandler ) item;
            }
        }
    }

    #endregion

    #region " validation methods "

    /// <summary>   Authenticates a user by user name and password. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userCredential">   The user credential. </param>
    /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    public abstract bool Authenticate( System.Net.NetworkCredential userCredential );

    /// <summary>   Authenticates a user by user name and password. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="userCredential">   The user credential. </param>
    /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    ///                                 the name of enumeration flags. </param>
    /// <returns>   <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    public virtual bool Authenticate( System.Net.NetworkCredential userCredential, ArrayList allowedUserRoles )
    {
        this.IsAuthenticated = this.TryFindUser( userCredential.UserName, allowedUserRoles ) && this.Authenticate( userCredential );
        return this.IsAuthenticated;
    }

    /// <summary> Tries to find a user in a group role. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="userName">         Specifies a user name. </param>
    /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    /// the name of enumeration flags. </param>
    /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c>. </returns>
    public abstract bool TryFindUser( string userName, ArrayList allowedUserRoles );

    /// <summary> Invalidates the last login user. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void Invalidate()
    {
        this.IsAuthenticated = false;
        this.Failed = false;
    }

    #endregion

    #region " validation outcomes "

    private string? _userName;

    /// <summary> Gets or sets (protected) the name of the user. </summary>
    /// <value> The name of the user. </value>
    public string? UserName
    {
        get => this._userName;

        protected set
        {
            this._userName = value;
            this.NotifyPropertyChanged();
        }
    }

    private ArrayList? _userRoles;

    /// <summary> Gets or sets (protected) the roles of the user. </summary>
    /// <value> The Role of the user. </value>
    public ArrayList? UserRoles
    {
        get => this._userRoles;

        protected set
        {
            this._userRoles = value;
            this.NotifyPropertyChanged( nameof( this.UserRoles ) );
        }
    }

    private bool _isAuthenticated;

    /// <summary>
    /// Gets or sets (protected) a value indicating whether the user is authenticated.
    /// </summary>
    /// <value> <c>true</c> if the user authenticated is valid; otherwise <c>false</c>. </value>
    public bool IsAuthenticated
    {
        get => this._isAuthenticated;

        protected set
        {
            this._isAuthenticated = value;
            this.NotifyPropertyChanged();
        }
    }

    private string? _validationDetails;

    /// <summary> Gets or sets (protected) the validation message. </summary>
    /// <value> A message describing the validation. </value>
    public string? ValidationMessage
    {
        get => this._validationDetails;

        protected set
        {
            this._validationDetails = value;
            this.NotifyPropertyChanged();
        }
    }

    private bool _failed;

    /// <summary> Gets or sets a value indicating whether the login failed. </summary>
    /// <value> <c>true</c> if failed; otherwise <c>false</c>. </value>
    public bool Failed
    {
        get => this._failed;

        protected set
        {
            this._failed = value;
            this.NotifyPropertyChanged();
        }
    }

    #endregion

    #region " manage users "

    /// <summary>   Adds a user to group. </summary>
    /// <remarks>   David, 2021-05-17. </remarks>
    /// <param name="principalContext"> Context for the principal. </param>
    /// <param name="userId">           Identifier for the user. </param>
    /// <param name="groupName">        Name of the group. </param>
    public static void AddUserToGroup( PrincipalContext principalContext, string userId, string groupName )
    {
        GroupPrincipal group = GroupPrincipal.FindByIdentity( principalContext, groupName );
        group.Members.Add( principalContext, IdentityType.UserPrincipalName, userId );
        group.Save();
    }

    /// <summary>   Removes the user from group. </summary>
    /// <remarks>   David, 2021-05-17. </remarks>
    /// <param name="principalContext"> Context for the principal. </param>
    /// <param name="userId">           Identifier for the user. </param>
    /// <param name="groupName">        Name of the group. </param>
    public static void RemoveUserFromGroup( PrincipalContext principalContext, string userId, string groupName )
    {
        GroupPrincipal group = GroupPrincipal.FindByIdentity( principalContext, groupName );
        _ = group.Members.Remove( principalContext, IdentityType.UserPrincipalName, userId );
        group.Save();
    }

    /// <summary>   Searches for the first user. </summary>
    /// <remarks>   David, 2021-05-17. </remarks>
    /// <param name="principalContext"> Context for the principal. </param>
    /// <param name="userId">           Identifier for the user. </param>
    /// <returns>   The found user. </returns>
    public static UserPrincipal FindUser( PrincipalContext principalContext, string userId )
    {
        return UserPrincipal.FindByIdentity( principalContext, userId );
    }

    /// <summary>   Creates a user. </summary>
    /// <remarks>   David, 2021-05-17. </remarks>
    /// <param name="principalContext"> Context for the principal. </param>
    /// <param name="userId">           Identifier for the user. </param>
    /// <param name="password">         The password. </param>
    /// <param name="userDescription">  (Optional) Information describing the user. </param>
    /// <returns>   The new user. </returns>
    public static UserPrincipal CreateUser( PrincipalContext principalContext, string userId, string password, string userDescription = "Created from .NET" )
    {
        UserPrincipal userPrincipal = UserPrincipal.FindByIdentity( principalContext, userId );
        if ( userPrincipal is null )
        {
            userPrincipal = new( principalContext )
            {
                Name = userId
            };
            userPrincipal.SetPassword( password );
            userPrincipal.Description = userDescription;
            userPrincipal.Save();
        }
        return userPrincipal;
    }

    #endregion

    #region " authenticate "

    /// <summary>   Try authenticate. </summary>
    /// <remarks>   David, 2021-05-17. </remarks>
    /// <param name="principalContext"> Context for the principal. </param>
    /// <param name="userCredential">   The user credential. </param>
    /// <param name="requiredGroup">    the group to which the user must belong. </param>
    /// <returns>   A Tuple. </returns>
    public static (bool Success, string Details) TryAuthenticate( PrincipalContext principalContext, System.Net.NetworkCredential userCredential, string requiredGroup )
    {
        return LoginBase.TryAuthenticate( principalContext, userCredential.UserName, userCredential.Password, requiredGroup );
    }

    /// <summary>   Try authenticate. </summary>
    /// <remarks>   David, 2021-05-13. </remarks>
    /// <param name="principalContext"> Context for the principal. </param>
    /// <param name="userId">           Identifier for the user. </param>
    /// <param name="password">         Specifies a password. </param>
    /// <param name="requiredGroup">    the group to which the user must belong. </param>
    /// <returns>   A Tuple. </returns>
    public static (bool Success, string Details) TryAuthenticate( PrincipalContext principalContext, string userId, string password, string requiredGroup )
    {
        try
        {
            using UserPrincipal? user = UserPrincipal.FindByIdentity( principalContext, userId );
            if ( user is null )
            {
                return (false, $"User '{userId}' not identified on this machine");
            }
            else
            {
                if ( principalContext.ValidateCredentials( userId, password ) )
                {
                    bool found = false;
                    foreach ( Principal group in user.GetAuthorizationGroups() )
                    {
                        if ( string.Equals( group.Name, requiredGroup, StringComparison.OrdinalIgnoreCase ) )
                        {
                            found = true;
                            break;
                        }
                    }
                    return (found, found ? string.Empty : $"User '{userId}' does not have the expected '{requiredGroup}' role");
                }
                else
                {
                    return (false, $"Failed validating credentials for user '{userId}'");
                }

            }
        }
        catch ( Exception ex )
        {
            return (false, ex.ToString());
        }
    }

    #endregion

    #region " user roles "

    /// <summary>   Enumerate user roles. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="user"> The user. </param>
    /// <returns>   An ArrayList. </returns>
    public static ArrayList EnumerateUserRoles( UserPrincipal user )
    {
        ArrayList list = [];
        if ( user is not null )
        {
            foreach ( Principal group in user.GetAuthorizationGroups() )
            {
                _ = list.Add( group.Name );
            }
        }

        return list;
    }

    /// <summary>   Enumerate user roles. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="user">             The user. </param>
    /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    ///                                 the name of enumeration flags. </param>
    /// <returns>   An ArrayList. </returns>
    public static ArrayList EnumerateUserRoles( UserPrincipal user, ArrayList allowedUserRoles )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( user, nameof( user ) );
        ArgumentNullException.ThrowIfNull( allowedUserRoles, nameof( allowedUserRoles ) );
#else
        if ( user is null )
        {
            throw new ArgumentNullException( nameof( user ) );
        }

        if ( allowedUserRoles is null )
        {
            throw new ArgumentNullException( nameof( allowedUserRoles ) );
        }
#endif

        ArrayList roles = [];
        foreach ( Principal group in user.GetAuthorizationGroups() )
        {
            if ( allowedUserRoles.Contains( group.Name ) )
            {
                _ = roles.Add( group.Name );
            }
        }
        return roles;
    }

    /// <summary>   Enumerate user roles. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="userRoles">        The user roles. </param>
    /// <param name="allowedUserRoles"> Specifies the list of valid roles. This could be compared to
    ///                                 the name of enumeration flags. </param>
    /// <returns>   An ArrayList. </returns>
    public static ArrayList EnumerateUserRoles( ArrayList userRoles, ArrayList allowedUserRoles )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( userRoles, nameof( userRoles ) );
        ArgumentNullException.ThrowIfNull( allowedUserRoles, nameof( allowedUserRoles ) );
#else
        if ( userRoles is null )
        {
            throw new ArgumentNullException( nameof( userRoles ) );
        }

        if ( allowedUserRoles is null )
        {
            throw new ArgumentNullException( nameof( allowedUserRoles ) );
        }
#endif

        ArrayList roles = [];
        foreach ( string role in userRoles )
        {
            if ( allowedUserRoles.Contains( role ) )
            {
                _ = roles.Add( role );
            }
        }

        return roles;
    }

    #endregion

    #region " domains "

    /// <summary>   Enumerate domains. </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <returns>   An ArrayList. </returns>
    public static ArrayList EnumerateDomains()
    {
        ArrayList domainNames = [];
        try
        {
            Forest currentForest = Forest.GetCurrentForest();
            DomainCollection myDomains = currentForest.Domains;
            foreach ( Domain domain in myDomains )
            {
                _ = domainNames.Add( domain.Name );
            }
        }
        catch ( ActiveDirectoryOperationException )
        {
            // this exception will occur if the Current security context is not associated with an Active Directory domain or forest.
            // so an empty list is returned.
        }

        return domainNames;
    }

    #endregion
}
